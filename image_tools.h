#include <stdint.h>

#ifndef __IMAGE_TOOLS_H__
#define __IMAGE_TOOLS_H__
static void	image_do_histogram_normalization(int16_t *imageTexels, const int width, const int height);
static void	image_morph_op_closing(uint8_t *imageTexels, const int width, const int height);
static void	image_apply_mask(int16_t *imageTexels, const uint8_t *maskTexels, const int width, const int height);
static void	image_adaptative_threshold(int16_t *dataPtr, int width, int height, uint8_t *thresholdedImage);
#endif

