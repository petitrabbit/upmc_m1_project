#!/bin/bash

echo "Cleaning stuff"
rm main.o
rm irm_nez

echo "Building App..."
gtk_deps=`pkg-config --cflags --libs gtk+-3.0`
gcc -m64 -Wall utilities.h image_tools.c image_tools.h main.c nifti1_io.c znzlib.c znzlib.h nifti1_io.h config.h -lGL -lm $gtk_deps -o main.o

echo "Linking object files"
link main.o irm_nez 

./irm_nez # TODO: a supprimer
