#include <stdint.h>

#ifndef __UTILITIES_H__
#define __UTILITIES_H__
static uint8_t 
utils_find_max_in_array(uint8_t* array, const int array_length)
{
    if ( array_length == 0 ) {
        return 0;
    }

    uint8_t max = array[0];
    
    int i;
    for (i = 1; i < array_length; i++) {
        max = array[i] > max ? array[i] : max;	
    }

    return max;
}

static int
utils_is_on_boundary(int i, int j, int width, int height, int halfSize)
{
	int boundary_test_1 = (i - halfSize) >= 0;
	int boundary_test_2 = (i + halfSize) < width;

	int boundary_test_3 = (j - halfSize) >= 0;
	int boundary_test_4 = (j + halfSize) < height;

	return (boundary_test_1 && boundary_test_2 && boundary_test_3 && boundary_test_4);
}
#endif

