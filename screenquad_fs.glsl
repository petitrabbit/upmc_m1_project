#version 130

in vec2 vs_TexCoordinates;

uniform sampler2D g_InputTexture;
uniform sampler2D g_InputTextureMask;

uniform int g_ToggleTextureMask;
uniform float g_InputTextureLuminosity;

out vec4 fs_Color;

void main()
{
    float intensity = texture2D(g_InputTexture, vs_TexCoordinates).r * g_InputTextureLuminosity;
    float mask = texture2D(g_InputTextureMask, vs_TexCoordinates).r;

    if (g_ToggleTextureMask == 1) {
        fs_Color = vec4(mask, 0, 0, 1);    
    } else {
        fs_Color = vec4(intensity, intensity, intensity, 1);
    }
}

