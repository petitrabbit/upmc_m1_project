#version 130

in vec3 vb_Position;
in vec2 vb_TexCoordinates;

uniform mat4 g_ModelViewProjectionMatrix;

out vec2 vs_TexCoordinates;

void main() 
{
  gl_Position = g_ModelViewProjectionMatrix * vec4(vb_Position, 1.0);
  
  vs_TexCoordinates = vb_TexCoordinates;
}




