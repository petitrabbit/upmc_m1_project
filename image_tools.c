#include <stdint.h>
#include "utilities.h"

#define USE_KERNEL_5x5

void
image_do_histogram_normalization(int16_t *imageTexels, const int width, const int height)
{
    int i, j, index;
    float texelNormalized = 0.0f;
    long frequencySum = 0;

    // Compute the sum of the frequencies of the image
    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++) {
            index = j * width + i;

            frequencySum += imageTexels[index];
        }	
    }

    // Normalize histogram
    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++) {
            index = j * width + i;

            // Remap from 0..1 to 0..255
            // Multiply by 250.0f to get a mask with low constrast (so that we don't keep noise from MRI)
            texelNormalized = ((imageTexels[index] / (float)frequencySum) * 255) * 250.0f;
            imageTexels[index] = texelNormalized;
        }
    }
}

void
image_morph_op_closing(uint8_t *imageTexels, const int width, const int height)
{
#ifndef USE_KERNEL_NONE
#if defined( USE_KERNEL_5x5 )
    static const int KERNEL_SIZE = 5;
    static const int KERNEL_HALF_SIZE = (5 - 1) / 2;
    static const char KERNEL[5][5] = {
        {0,0,1,0,0},
        {0,1,1,1,0},
        {1,1,1,1,1},
        {0,1,1,1,0},
        {0,0,1,0,0}
    };
#elif defined( USE_KERNEL_3x3 )
    static const int KERNEL_SIZE = 3;
    static const int KERNEL_HALF_SIZE = ( 3 - 1) / 2;
    static const char KERNEL[3][3] = {
        {0,1,0},
        {1,1,1},
        {0,1,0},
    };
#endif

    uint8_t texelList[KERNEL_SIZE*KERNEL_SIZE];
    uint8_t texelValue;
    int i, j, index, x, y, texelIndex;

    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++) {
            index = j * width + i;

            if ( utils_is_on_boundary(i, j, width, height, KERNEL_HALF_SIZE) ) {
                // Reset texel list
                texelIndex = 0;
                memset(texelList, 0, sizeof(uint8_t) * (KERNEL_SIZE*KERNEL_SIZE));

                for (x = 0; x < KERNEL_SIZE; x++) {
                    for (y = 0; y < KERNEL_SIZE; y++) {
                        if (KERNEL[x][y] == 1) {
                            texelList[texelIndex] = imageTexels[(j+y) * width + (i+x)];
                            texelIndex++;
                        }				
                    }			
                }

                texelValue = utils_find_max_in_array(texelList, texelIndex);
                imageTexels[index] = texelValue;
            }
        }
    }
#endif
}

void
image_apply_mask(int16_t *imageTexels, const uint8_t *maskTexels, const int width, const int height)
{
    int i, j, index;

    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++) {
            index = j * width + i;

            // Do a comparison between the binarized image and the original data (the binarized image acts like a mask)
            imageTexels[index] = (maskTexels[index] == 255) ? imageTexels[index] : 0;
        }	
    }
}

void
image_adaptative_threshold(int16_t *dataPtr, int width, int height, uint8_t *thresholdedImage)
{    
    unsigned long* integralImg;   
    int i, j;
    long sum = 0;
    int index;
    int count = 0;
    int x1, y1, x2, y2;
    int s2 = (width / 8 ) / 2;

    // Create the integral image
    integralImg = (unsigned long*)malloc(width * height * sizeof(unsigned long));

    for (i = 0; i < width; i++) {
        // Reset column sum
        sum = 0;

        for (j = 0; j < height; j++) {
            index = j * width + i;

            sum += dataPtr[index];
            integralImg[index] = (i == 0) ? sum : integralImg[index - 1] + sum;
        }
    }

    // perform thresholding
    for (i = 0; i < width; i++) {
        for (j = 0; j < height; j++) {
            index = j * width + i;

            // set the SxS region
            x1 = i - s2; 
            x2 = i + s2;

            y1 = j - s2; 
            y2 = j + s2;

            // check the border
            if (x1 < 0) x1 = 0;
            if (x2 >= width) x2 = width-1;

            if (y1 < 0) y1 = 0;
            if (y2 >= height) y2 = height-1;

            count = (x2 - x1) * (y2 - y1);

            // I(x,y)=s(x2,y2)-s(x1,y2)-s(x2,y1)+s(y1,x1)
            sum = integralImg[y2*width+x2] 
                - integralImg[y1*width+x2] 
                - integralImg[y2*width+x1]
                + integralImg[y1*width+x1];

            static const float THRESHOLD = 1e-4f;

            if ((long)(dataPtr[index] * count) < (unsigned long)(sum * (1.0 - THRESHOLD)))
                thresholdedImage[index] = 255;
            else
                thresholdedImage[index] = 0;
        }
    }

    free(integralImg);
}

