#include <gtk/gtk.h>
#include <GL/gl.h>
#include <stdio.h>
#include "nifti1_io.h"

#include "image_tools.h"

#define APPLICATION_NAME 		"MRI Nose Segmentation"
#define WINDOW_INITIAL_WIDTH 	640
#define WINDOW_INITIAL_HEIGHT 	480

static GtkApplication   *g_ApplicationInstance;
static GtkWidget        *g_WindowInstance;

static GtkWidget        *g_SliceSelection;
static GtkWidget 	    *g_RenderArea;
static GtkWidget        *g_LuminositySelection;

static GLuint           *g_CurrentTexture2DArray;
static GLuint           *g_CurrentTextureMask2DArray;
static GLint            g_ActiveTextureSlice;

static double           g_RenderAreaWidth = 0.0;
static double           g_RenderAreaHeight = 0.0;

static struct {
    guint Handle;
    
    struct {
        guint ModelViewProjectionMatrix;
        guint InputTextureSampler;
	    guint InputTextureMaskSampler;
	    guint ToggleTextureMask;
	    guint InputTextureLuminosity;
    } Uniforms;
    
    struct {
        guint Position;
        guint TextureCoordinates;
    } VertexBuffer;
} g_RenderProgramData;

static GLuint       *g_ScreenQuadVertexBufferHandle;
static float        g_ModelViewProjectionMatrix[16];
static GLint		g_ToggleTextureMask 		= FALSE;
static float		g_InputTextureLuminosity 	= 1.0f;

static nifti_image  *g_CurrentImage = NULL;
static double		g_NoseAreaOfInterest[4];
static int		    g_NoseAreaOfInterestPoints[4][2];
static int			g_AreaOfInterestCurrentId	= 0;

static void
init_mvp (float *res)
{
    /* initialize a matrix as an identity matrix */
    res[0] = 1.f; res[4] = 0.f;  res[8] = 0.f; res[12] = 0.f;
    res[1] = 0.f; res[5] = 1.f;  res[9] = 0.f; res[13] = 0.f;
    res[2] = 0.f; res[6] = 0.f; res[10] = 1.f; res[14] = 0.f;
    res[3] = 0.f; res[7] = 0.f; res[11] = 0.f; res[15] = 1.f;
}

static void
apply_zone_of_interest()
{
    if ( g_CurrentImage == NULL ) {
        return;
    }

    int16_t* data_offset, *data_masked;
    int singleSliceDimension, singleSliceSize, sliceCount, i;
    int j, k, index;

    sliceCount = g_CurrentImage->nz;
    singleSliceDimension = g_CurrentImage->nx * g_CurrentImage->ny;
    singleSliceSize = singleSliceDimension * g_CurrentImage->nbyper;

    printf("%s >> singleSliceDimension=%i (in texels)\t bytePerVoxel=%i width=%i height=%i depth=%i\n", 
            __FUNCTION__, singleSliceDimension, g_CurrentImage->nbyper, g_CurrentImage->nx, g_CurrentImage->ny, sliceCount );
    
    data_masked = malloc(singleSliceSize);
    for(i = 0; i < sliceCount; i++) {
        data_offset = (int16_t*)(g_CurrentImage->data + i * singleSliceSize);
        
        memcpy(data_masked, data_offset, singleSliceSize);        
        
        // Test if the point is outside the zone of interest; if so, mask it (set its pixel value to 0)
        // 0 ------- 1
        // |         |
        // |         |
        // |         |
        // |         |
        // 2 ------- 3
        for ( j = 0; j < g_CurrentImage->nx; j++ ) {
            for ( k = 0; k < g_CurrentImage->ny; k++ ) {
                    if ( ( j < g_NoseAreaOfInterestPoints[0][0] )
                      || ( j < g_NoseAreaOfInterestPoints[2][0] )
                      || ( j > g_NoseAreaOfInterestPoints[1][0] )
                      || ( j > g_NoseAreaOfInterestPoints[3][0] )
                      || ( k < g_NoseAreaOfInterestPoints[0][1] )
                      || ( k < g_NoseAreaOfInterestPoints[1][1] )
                      || ( k > g_NoseAreaOfInterestPoints[2][1] )
                      || ( k > g_NoseAreaOfInterestPoints[3][1] ) ) {                     
                        index = k * g_CurrentImage->nx + j;
                        data_masked[index] = 0;
                    }                   
            }        
        }

        // Bind the texture slice to create
        glBindTexture(GL_TEXTURE_2D, g_CurrentTexture2DArray[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RG, g_CurrentImage->nx, g_CurrentImage->ny, 0, GL_RG, GL_UNSIGNED_BYTE, data_masked);
        
        // Setup sampler parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	}

    free(data_masked);
    gtk_gl_area_queue_render(g_RenderArea);
}

static void
load_texture_array_2d(nifti_image *loadedImage)
{
    int8_t* data_offset, *data_normalized;
    uint8_t* data_thresholded;

    int singleSliceDimension, singleSliceSize, sliceCount, i;
    GLenum data_type, gl_error;
    
    // Allocate glTexture2d handles for each slice
    g_CurrentTexture2DArray = (GLuint *)malloc(loadedImage->nz * sizeof(GLuint));

    // For debug purposes, store image mask
    g_CurrentTextureMask2DArray = (GLuint *)malloc(loadedImage->nz * sizeof(GLuint));

    glGenTextures(loadedImage->nz, g_CurrentTexture2DArray);
    glGenTextures(loadedImage->nz, g_CurrentTextureMask2DArray);

    gl_error = glGetError();
    if(gl_error != 0){
        printf("%s >> glGenTextures raised error id : %i @ slice : %i\n", __FUNCTION__, gl_error, i);
    }

    // Set proper data type
    switch (loadedImage->datatype) {
    case DT_UNSIGNED_CHAR:
        data_type = GL_UNSIGNED_BYTE;
        break;
    case DT_SIGNED_SHORT:
        data_type = GL_SHORT;
        break;
    case DT_SIGNED_INT:
        data_type = GL_INT;
        break;
    case DT_FLOAT:
        data_type = GL_FLOAT;
        break;
    case DT_COMPLEX:
    case DT_DOUBLE:
        data_type = GL_DOUBLE;
        break;  
    default:
        data_type = GL_SHORT;
        break;
    }
    
    sliceCount = loadedImage->nz;
    singleSliceDimension = loadedImage->nx * loadedImage->ny;
    singleSliceSize = singleSliceDimension * loadedImage->nbyper;

    printf("%s >> singleSliceDimension=%i (in texels)\t bytePerVoxel=%i width=%i height=%i depth=%i\n", 
            __FUNCTION__, singleSliceDimension, loadedImage->nbyper, loadedImage->nx, loadedImage->ny, sliceCount );
    
    data_thresholded = (uint8_t*)malloc( singleSliceSize );
    data_normalized = (int8_t*)malloc( singleSliceSize );

    for(i = 0; i < sliceCount; i++) {
        data_offset = (int8_t*)(loadedImage->data + i * singleSliceSize);
	
        memcpy(data_normalized, data_offset, singleSliceSize);        

        // Normalize histogram
        image_do_histogram_normalization(data_normalized, loadedImage->nx, loadedImage->ny);   

        // Do thresholding
        image_adaptative_threshold(data_normalized, loadedImage->nx, loadedImage->ny, data_thresholded);
            
        // Apply closing morphological operator
        image_morph_op_closing(data_thresholded, loadedImage->nx, loadedImage->ny);

        // Apply binary mask on the original image
        image_apply_mask(data_offset, data_thresholded, loadedImage->nx, loadedImage->ny);

        // Bind the texture slice to create
        glBindTexture(GL_TEXTURE_2D, g_CurrentTexture2DArray[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RG, loadedImage->nx, loadedImage->ny, 0, GL_RG, GL_UNSIGNED_BYTE, data_offset);
        
        gl_error = glGetError();
        if(gl_error != 0){
            printf("%s >> glTexImage2D raised error id : %i @ slice : %i\n", __FUNCTION__, gl_error, i);
        }
        
        // Setup sampler parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

	    // Create mask texture
        glBindTexture(GL_TEXTURE_2D, g_CurrentTextureMask2DArray[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, loadedImage->nx, loadedImage->ny, 0, GL_RED, GL_UNSIGNED_BYTE, data_thresholded);
	
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    }

    free(data_thresholded);
    free(data_normalized);

    gtk_range_set_value(g_SliceSelection, 0);
    gtk_gl_area_queue_render(g_RenderArea);
}

static guint
create_shader(int shader_type, const char *source)
{
    guint shader = glCreateShader(shader_type);
    
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);

    int status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        int log_length;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);

        char *buffer = g_malloc(log_length + 1);
        glGetShaderInfoLog(shader, log_length, NULL, buffer);

        printf("%s >> shader compilation FAILED!\nshader_type= %s\nlog_infos=%s\n", __FUNCTION__, shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment", buffer);
        
        g_free(buffer);
        glDeleteShader(shader);
    }
        
    return shader;
}

static char*
readcontent(const char *filename)
{
    char *fcontent = NULL;
    int fsize = 0;
    FILE *fp;

    fp = fopen(filename, "r");
    if(fp) {
        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        rewind(fp);

        fcontent = (char*) malloc(sizeof(char) * fsize);
        fread(fcontent, 1, fsize, fp);

        fclose(fp);
    }
    return fcontent;
}

static gboolean
init_shaders()
{
    char *source;
    guint vertex = 0, fragment = 0;

    /* load the vertex shader */
    source = readcontent ("screenquad_vs.glsl");
    vertex = create_shader(GL_VERTEX_SHADER, source);
    free(source);

    if (vertex == 0) goto out;

    source = readcontent ("screenquad_fs.glsl");
    fragment = create_shader(GL_FRAGMENT_SHADER, source);
    free(source);

    if (fragment == 0) goto out;

    /* link the vertex and fragment shaders together */
    g_RenderProgramData.Handle = glCreateProgram();
    glAttachShader(g_RenderProgramData.Handle, vertex);
    glAttachShader(g_RenderProgramData.Handle, fragment);
    glLinkProgram(g_RenderProgramData.Handle);

    int status = 0;
    glGetProgramiv(g_RenderProgramData.Handle, GL_LINK_STATUS, &status);
    
    if (status == GL_FALSE) {
        int log_length;
        glGetProgramiv(g_RenderProgramData.Handle, GL_INFO_LOG_LENGTH, &log_length);

        char *buffer = g_malloc(log_length + 1);
        glGetProgramInfoLog(g_RenderProgramData.Handle, log_length, NULL, buffer);

        printf("%s >> program linking FAILED!\nlog_infos=%s\n", __FUNCTION__, buffer);
        
        g_free(buffer);
        glDeleteProgram(g_RenderProgramData.Handle);
        
        g_RenderProgramData.Handle = 0;

        goto out;
    }
    
    // Retrieve uniforms handle
    g_RenderProgramData.Uniforms.ModelViewProjectionMatrix = glGetUniformLocation(g_RenderProgramData.Handle, "g_ModelViewProjectionMatrix");
    g_RenderProgramData.Uniforms.InputTextureSampler = glGetUniformLocation(g_RenderProgramData.Handle, "g_InputTexture");
    g_RenderProgramData.Uniforms.InputTextureMaskSampler = glGetUniformLocation(g_RenderProgramData.Handle, "g_InputTextureMask");
    g_RenderProgramData.Uniforms.ToggleTextureMask = glGetUniformLocation(g_RenderProgramData.Handle, "g_ToggleTextureMask");
    g_RenderProgramData.Uniforms.InputTextureLuminosity = glGetUniformLocation(g_RenderProgramData.Handle, "g_InputTextureLuminosity");

    // Retrieve vertex buffer input handle
    g_RenderProgramData.VertexBuffer.Position = glGetAttribLocation(g_RenderProgramData.Handle, "vb_Position");
    g_RenderProgramData.VertexBuffer.TextureCoordinates = glGetAttribLocation(g_RenderProgramData.Handle, "vb_TexCoordinates");

    // Cleaning
    glDetachShader(g_RenderProgramData.Handle, vertex);
    glDetachShader(g_RenderProgramData.Handle, fragment);

out:
    if (vertex != 0) glDeleteShader(vertex);
    if (fragment != 0) glDeleteShader(fragment);

    return g_RenderProgramData.Handle != 0;
}

struct VertexLayout 
{
  float Position[3];
  float TexCoordinates[2];
};

static const struct VertexLayout g_ScreenQuadData[] = 
{
  { {  -1.0f, -1.0f, 0.0f }, { 0, 0 } },
  { {  1.0f, -1.0f, 0.0f }, { 1, 0 } },
  { { 1.0f, 1.0f, 0.0f }, { 1, 1 } },
  { {  -1.0f, -1.0f, 0.0f }, { 0, 0 } },
  { { 1.0f, 1.0f, 0.0f }, { 1, 1 } },
  { { -1.0f, 1.0f, 0.0f }, { 0, 1 } },
};

static gboolean
init_buffers()
{
  guint buffer;

  glGenVertexArrays(1, &g_ScreenQuadVertexBufferHandle);
  glBindVertexArray(g_ScreenQuadVertexBufferHandle);

  /* this is the VBO that holds the vertex data */
  glGenBuffers(1, &buffer);
  glBindBuffer(GL_ARRAY_BUFFER, buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(g_ScreenQuadData), g_ScreenQuadData, GL_STATIC_DRAW);

  /* enable and set the position attribute */
  glEnableVertexAttribArray(g_RenderProgramData.VertexBuffer.Position);
  glVertexAttribPointer(g_RenderProgramData.VertexBuffer.Position, 3, GL_FLOAT, GL_FALSE,
                        sizeof(struct VertexLayout),
                        (GLvoid *)(G_STRUCT_OFFSET(struct VertexLayout, Position)));

  glEnableVertexAttribArray(g_RenderProgramData.VertexBuffer.TextureCoordinates);
  glVertexAttribPointer(g_RenderProgramData.VertexBuffer.TextureCoordinates, 2, GL_FLOAT, GL_FALSE,
                        sizeof (struct VertexLayout),
                        (GLvoid *)(G_STRUCT_OFFSET(struct VertexLayout, TexCoordinates)));

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  glDeleteBuffers(1, &buffer);
  
  return g_ScreenQuadVertexBufferHandle != 0;
}

static void
on_slice_selection_updated(GtkRange *caller, gpointer user_data)
{
    g_ActiveTextureSlice = (int)gtk_range_get_value(caller);
    gtk_gl_area_queue_render(g_RenderArea);
}


static void
on_luminosity_selection_updated(GtkRange *caller, gpointer user_data)
{
    g_InputTextureLuminosity = (float)gtk_range_get_value(caller);

    printf("%s >> g_InputTextureLuminosity = %.3f\n", __FUNCTION__, g_InputTextureLuminosity);

    gtk_gl_area_queue_render(g_RenderArea);
}

static void
realize(GtkGLArea *area)
{
    const char *renderer;
    GError *operation_result;
    gboolean init_result;
    
    gtk_gl_area_make_current(area);
    operation_result = gtk_gl_area_get_error(area);
    
    // Check if internal context creation has failed
    if (operation_result != NULL) {
        printf("%s >> gtk_gl_area_make_current : %s\n", __FUNCTION__, operation_result->message);
        return;
    }

    // Create resources for rendering
    init_result = init_shaders();
    if (!init_result) {
        printf("%s >> init_shaders FAILED\n", __FUNCTION__);
        return;
    }
    
    init_result = init_buffers();
    if (!init_result) {
        printf("%s >> init_buffers FAILED\n", __FUNCTION__);
        return;
    }
    
    renderer = (char *)glGetString(GL_RENDERER);   
    printf("%s >> selected renderer %s\n", __FUNCTION__, renderer != NULL ? renderer : "UNKNOWN");
    
    init_mvp(&g_ModelViewProjectionMatrix);
}

static gboolean
render(GtkGLArea *area, GdkGLContext *context)
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    
    if (g_RenderProgramData.Handle == 0 || g_CurrentTexture2DArray == NULL) return FALSE;

    glUseProgram(g_RenderProgramData.Handle);
    // GLenum gl_error = glGetError();
    // if(gl_error != 0){
        // printf("%s >> something raised error id : %i\n", __FUNCTION__, gl_error);
    // }

    glUniformMatrix4fv(g_RenderProgramData.Uniforms.ModelViewProjectionMatrix, 1, GL_FALSE, &(g_ModelViewProjectionMatrix[0]));
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, g_CurrentTexture2DArray[g_ActiveTextureSlice]);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, g_CurrentTextureMask2DArray[g_ActiveTextureSlice]);
   
    glUniform1i(g_RenderProgramData.Uniforms.InputTextureSampler, 0);
    glUniform1i(g_RenderProgramData.Uniforms.InputTextureMaskSampler, 1);
    glUniform1i(g_RenderProgramData.Uniforms.ToggleTextureMask, (int)g_ToggleTextureMask);    
    glUniform1fv(g_RenderProgramData.Uniforms.InputTextureLuminosity, 1, &g_InputTextureLuminosity);

    glBindVertexArray(g_ScreenQuadVertexBufferHandle);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);
  
    return TRUE;
}

static void
on_quit_activated(GtkWidget *caller)
{
    g_application_quit(G_APPLICATION(g_ApplicationInstance));
}

static void
on_file_open_activated(GtkWidget *caller)
{
    GtkWidget *dialog;
    gint result;  

    const GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;

    dialog = gtk_file_chooser_dialog_new(
        "Open File", 
        g_WindowInstance, 
        action, 
        ("_Cancel"), GTK_RESPONSE_CANCEL,
        ("_Open"), GTK_RESPONSE_ACCEPT,
        NULL
    );
    
    result = gtk_dialog_run(GTK_DIALOG(dialog));

    if (result == GTK_RESPONSE_ACCEPT) {
        char *selectedFilename;

        GtkFileChooser *fileChooser = GTK_FILE_CHOOSER(dialog);
        selectedFilename = gtk_file_chooser_get_filename(fileChooser);

        printf("%s >> file choosen: %s\n", __FUNCTION__, selectedFilename);

        g_CurrentImage = nifti_image_read(selectedFilename, 1);

        if (g_CurrentImage == NULL) {
            printf("%s >> failed to load nifti image %s (loadedImage was NULL)\n", __FUNCTION__, selectedFilename);
        } else {
            printf("%s >> nifti_image_read was successful; now uploading texture to the GPU...\n", __FUNCTION__);

	        gtk_widget_set_sensitive(g_SliceSelection, TRUE);
	        gtk_widget_set_sensitive(g_LuminositySelection, TRUE);
	        gtk_range_set_range(g_SliceSelection, 0, g_CurrentImage->nz);
	        load_texture_array_2d(g_CurrentImage);
        }

        g_free(selectedFilename);
    }

    gtk_widget_destroy(dialog);
}

static void
on_mask_toggle_update(GtkToggleButton *caller, gpointer user_data)
{
	g_ToggleTextureMask = !g_ToggleTextureMask;
    	gtk_gl_area_queue_render(g_RenderArea);
}

static gboolean
on_render_area_click(GtkWidget *caller, GdkEventButton *event, gpointer user_data)
{
    if (event->type == GDK_BUTTON_PRESS && event->button == 1) {
		// Reset current area and reloop
		if ( g_AreaOfInterestCurrentId >= 4 ) {
			printf("debug : reset\n" );

			g_NoseAreaOfInterest[0] = 0.0;
			g_NoseAreaOfInterest[1] = 0.0;
			g_NoseAreaOfInterest[2] = 0.0;
			g_NoseAreaOfInterest[3] = 0.0;

			g_AreaOfInterestCurrentId = 0;
		}

		// Update area point
		// Since the render area dimension is different than the mri, convert the point to 0..1 range
		g_NoseAreaOfInterest[g_AreaOfInterestCurrentId] = event->x / g_RenderAreaWidth;
		g_NoseAreaOfInterest[g_AreaOfInterestCurrentId + 1] = fabs( 1.0 - event->y / g_RenderAreaHeight );
		
		g_AreaOfInterestCurrentId += 2;


		// We have enough point to build the area of interest		
		if ( g_AreaOfInterestCurrentId >= 4 ) {
			printf(" debug : build area\n");
    
            // 0 ------- 1
            // |         |
            // |         |
            // |         |
            // |         |
            // 2 ------- 3
            // Convert point cordinates from 0..1 to 0..x/y (image dimension)
            g_NoseAreaOfInterestPoints[0][0] = (int)floor( g_NoseAreaOfInterest[0] * g_CurrentImage->nx );
            g_NoseAreaOfInterestPoints[2][1] = (int)floor( g_NoseAreaOfInterest[1] * g_CurrentImage->ny );

            g_NoseAreaOfInterestPoints[1][0] = (int)floor( g_NoseAreaOfInterest[2] * g_CurrentImage->nx );
            g_NoseAreaOfInterestPoints[3][1] = (int)floor( g_NoseAreaOfInterest[1] * g_CurrentImage->ny );

            g_NoseAreaOfInterestPoints[2][0] = (int)floor( g_NoseAreaOfInterest[0] * g_CurrentImage->nx );
            g_NoseAreaOfInterestPoints[0][1] = (int)floor( g_NoseAreaOfInterest[3] * g_CurrentImage->ny );

            g_NoseAreaOfInterestPoints[3][0] = (int)floor( g_NoseAreaOfInterest[2] * g_CurrentImage->nx );
            g_NoseAreaOfInterestPoints[1][1] = (int)floor( g_NoseAreaOfInterest[3] * g_CurrentImage->ny );

        printf( "debug : zone of interest %i %i ; %i %i ; %i %i ; %i %i\n", g_NoseAreaOfInterestPoints[0][0], g_NoseAreaOfInterestPoints[0][1], g_NoseAreaOfInterestPoints[1][0], g_NoseAreaOfInterestPoints[1][1], g_NoseAreaOfInterestPoints[2][0], g_NoseAreaOfInterestPoints[2][1], g_NoseAreaOfInterestPoints[3][0], g_NoseAreaOfInterestPoints[3][1] );
			// Mask the image
            apply_zone_of_interest();
		}
        return TRUE;
    }

    return FALSE;
}

static void 
render_area_get_size(GtkWidget *caller, GtkAllocation *allocation, void *data) {
    g_RenderAreaWidth = allocation->width;
    g_RenderAreaHeight = allocation->height;
}

static void
activate(GtkApplication *app, gpointer user_data)
{
    // Menu Bar
    GtkWidget *widget_grid;
    GtkWidget *fileMenu;
    GtkWidget *menuBar;
    GtkWidget *fileOpenMenuItem, *editMenuItem;
    GtkWidget *quitMenuItem;
    GtkWidget *fileOpenDialog;
    GtkWidget *sliderLabel;
    GtkWidget *sliderLuminosityLabel;
    GtkWidget *debugToggleMask;

    // Setup Window
    g_WindowInstance = gtk_application_window_new(app);

    gtk_window_set_position(GTK_WINDOW(g_WindowInstance), GTK_WIN_POS_CENTER);
    gtk_window_set_title(GTK_WINDOW(g_WindowInstance), APPLICATION_NAME);
    gtk_window_set_default_size(GTK_WINDOW(g_WindowInstance), WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT);

    // Setup widget grid  
    widget_grid = gtk_grid_new();
    g_object_set (widget_grid, "row_homogeneous", TRUE);
    g_object_set (widget_grid, "column_homogeneous", TRUE);
     
    gtk_container_add(GTK_CONTAINER(g_WindowInstance), widget_grid);

    menuBar = gtk_menu_bar_new();
    
    fileMenu = gtk_menu_new();
    
    // Labels & menus
    fileOpenMenuItem = gtk_menu_item_new_with_label("File");
    fileOpenDialog = gtk_menu_item_new_with_label("Open");
    quitMenuItem = gtk_menu_item_new_with_label("Quit");

    // Add to menu
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(fileOpenMenuItem), fileMenu);

    // Add to submenus
    gtk_menu_shell_append(GTK_MENU_SHELL(fileMenu), fileOpenDialog);
    gtk_menu_shell_append(GTK_MENU_SHELL(fileMenu), quitMenuItem);
    
    // Add to menubar
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar), fileOpenMenuItem);
    
    gtk_grid_attach (GTK_GRID (widget_grid), menuBar, 0, 0, 1, 1);
    //gtk_box_pack_start(GTK_BOX(verticalBox), menuBar, FALSE, FALSE, 0);

    //gtk_grid_attach_next_to(GTK_GRID (widget_grid), render_area, menuBar, GTK_POS_TOP, 1, 9);
    //gtk_box_pack_start(GTK_BOX(verticalBox), render_area, TRUE, TRUE, 0);

    // Create Slice Selection Slider
    sliderLabel = gtk_label_new("Slice:");
    gtk_grid_attach (GTK_GRID (widget_grid), sliderLabel, 0, 8, 1, 1);

    g_SliceSelection = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 255, 1);
    gtk_grid_attach (GTK_GRID (widget_grid), g_SliceSelection, 1, 8, 1, 1);

    sliderLuminosityLabel = gtk_label_new("Luminosity:");
    gtk_grid_attach (GTK_GRID (widget_grid), sliderLuminosityLabel, 0, 9, 1, 1);
    g_LuminositySelection = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 100.0f, 0.1);
    gtk_grid_attach (GTK_GRID (widget_grid), g_LuminositySelection, 1, 9, 1, 1);

    //gtk_grid_attach (GTK_GRID (widget_grid), g_SliceSelection, 0, 9, 10, 1);
    //gtk_box_pack_start(GTK_BOX(verticalBox), g_SliceSelection, FALSE, FALSE, 0);
    
    debugToggleMask = gtk_check_button_new_with_label("Toggle Texture Mask");
    g_signal_connect(G_OBJECT(debugToggleMask), "toggled", G_CALLBACK(on_mask_toggle_update), NULL);
    gtk_grid_attach (GTK_GRID (widget_grid), debugToggleMask, 2, 0, 1, 1);


    // Create OpenGL render area (core context)
    g_RenderArea = gtk_gl_area_new();
    gtk_widget_set_events(GTK_WIDGET(g_RenderArea),
		        GDK_EXPOSURE_MASK|
		        GDK_BUTTON_PRESS_MASK|
		        GDK_BUTTON_RELEASE_MASK|
		        GDK_KEY_PRESS_MASK|
		        GDK_KEY_RELEASE_MASK|
			    GDK_POINTER_MOTION_MASK|
			    GDK_POINTER_MOTION_HINT_MASK);
    g_signal_connect(g_RenderArea, "realize", G_CALLBACK(realize), NULL);
    g_signal_connect(g_RenderArea, "render", G_CALLBACK(render), NULL);
    g_signal_connect(g_RenderArea, "button_press_event", G_CALLBACK(on_render_area_click), NULL);
    g_signal_connect(g_RenderArea, "size-allocate", G_CALLBACK(render_area_get_size), NULL);
    gtk_grid_attach (GTK_GRID (widget_grid), g_RenderArea, 1, 0, 1, 8);

    // Don't use auto rendering (since we want to render on user input)
    gtk_gl_area_set_auto_render(g_RenderArea, FALSE);

    g_signal_connect(G_OBJECT(g_SliceSelection), "value-changed", G_CALLBACK(on_slice_selection_updated), NULL);
    g_signal_connect(G_OBJECT(g_LuminositySelection), "value-changed", G_CALLBACK(on_luminosity_selection_updated), NULL);
    
    // Quit submenu item callback
    g_signal_connect(G_OBJECT(quitMenuItem), "activate", G_CALLBACK(on_quit_activated), NULL);

    // Open File dialog callback
    g_signal_connect(G_OBJECT(fileOpenDialog), "activate", G_CALLBACK(on_file_open_activated), NULL);

    gtk_range_set_value(g_LuminositySelection, g_InputTextureLuminosity);
    gtk_widget_set_sensitive(g_SliceSelection, FALSE);
    gtk_widget_set_sensitive(g_LuminositySelection, FALSE);
   

    // Show all
    gtk_widget_show_all(g_WindowInstance);
}

int
main(int argc, char **argv)
{
  int status;

  g_ApplicationInstance = gtk_application_new("org.gtk.mrinoseviewer", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(g_ApplicationInstance, "activate", G_CALLBACK(activate), NULL);

  status = g_application_run(G_APPLICATION(g_ApplicationInstance), argc, argv);

  g_object_unref(g_ApplicationInstance);

  return status;
}
